package com.rostyslavprotsiv;

import com.rostyslavprotsiv.model.entity.lock.MyReadWriteLock;

import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MyReadWriteLockReentrant {
//    private static ReadWriteLock rwLock = new MyReadWriteLock();
    private static ReadWriteLock rwLock = new ReentrantReadWriteLock();
    private static Lock readLock = rwLock.readLock();
    private static Lock writeLock = rwLock.writeLock();
    private static long A = 0; // Shared Resource

    static class MyThread implements Runnable {
        @Override
        public void run() {
            readLock.lock();
            try {
                System.out.println(LocalDateTime.now() + Thread.currentThread().getName() + " A=" + A);
                Thread.sleep(2000);
            } catch (InterruptedException e) {
            } finally {
                readLock.unlock();
            }
        }
    }

        public static void main(String[] args) {
            ExecutorService executor = Executors.newFixedThreadPool(4);
            Runnable rT1 = new MyThread();
            Runnable rT2 = new MyThread();
            Runnable rT3 = new MyThread();
            executor.submit(rT1);
            executor.submit(rT2);
            executor.submit(rT3);
            executor.submit(() -> {
                writeLock.lock();
                try {
                    A = 25;
                    System.out.println(LocalDateTime.now() + Thread.currentThread().getName() + "W A=" + A);
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                } finally {
                    writeLock.unlock();
                }
            });
            executor.submit(rT1);
            executor.submit(rT2);
            executor.submit(rT3);
            executor.shutdown();
        }
    }
