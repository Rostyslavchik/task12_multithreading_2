package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.BlockingQueueCommunication;
import com.rostyslavprotsiv.model.action.LockActionTest;
import com.rostyslavprotsiv.view.Menu;

public class Controller {
    private static final LockActionTest LOCK_ACTION_TEST = new LockActionTest();
    private static final Menu MENU = new Menu();
    private static final BlockingQueueCommunication BLOCKING_QUEUE_COMMUNICATION
            = new BlockingQueueCommunication();

    public void execute() {
        MENU.welcome();
        MENU.outTestWithGlobalLock(LOCK_ACTION_TEST.testWithGlobalLock());
        MENU.outTestWithoutGlobalLock(LOCK_ACTION_TEST.testWithoutGlobalLock());
        MENU.outBlockingQueueCommunicationResult();
        BLOCKING_QUEUE_COMMUNICATION.testCommunication();
    }
}
