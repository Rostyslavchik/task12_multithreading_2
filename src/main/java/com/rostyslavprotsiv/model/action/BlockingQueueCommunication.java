package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.queuetask.BlockingQueueReader;
import com.rostyslavprotsiv.model.entity.queuetask.BlockingQueueWriter;

import java.util.concurrent.*;

public class BlockingQueueCommunication {

    public void testCommunication() {
//        BlockingQueue<Character> blockingQueue = new LinkedBlockingQueue<>();
        BlockingQueue<Character> blockingQueue = new ArrayBlockingQueue<>(4);
        BlockingQueueWriter writer = new BlockingQueueWriter(blockingQueue
                , "Abalavaa");
        BlockingQueueReader reader = new BlockingQueueReader(blockingQueue);
        ExecutorService service = Executors.newFixedThreadPool(2);
        service.execute(writer);
        service.execute(reader);
        service.shutdown();
    }
}
