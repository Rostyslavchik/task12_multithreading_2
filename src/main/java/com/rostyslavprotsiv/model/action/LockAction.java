package com.rostyslavprotsiv.model.action;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockAction {
    private int counter = 0;
    private static final Lock LOCK = new ReentrantLock();

    public int getCounter() {
        return counter;
    }

    public void incrC1() {
        LOCK.lock();
        for (int i = 0; i < 100000; i ++) {
            counter++;
        }
        LOCK.unlock();
    }

    public void incrC2() {
        LOCK.lock();
        for (int i = 0; i < 100000; i ++) {
            counter++;
        }
        LOCK.unlock();
    }

    public void incrC3() {
        LOCK.lock();
        for (int i = 0; i < 100000; i ++) {
            counter++;
        }
        LOCK.unlock();
    }

    public void incrC4() {
        Lock badLock = new ReentrantLock();
        badLock.lock();
            for (int i = 0; i < 100000; i ++) {
                counter++;
            }
        badLock.unlock();
    }

    public void incrC5() {
        Lock badLock = new ReentrantLock();
        badLock.lock();
            for (int i = 0; i < 100000; i ++) {
                counter++;
            }
        badLock.unlock();
    }

    public void incrC6() {
        Lock badLock = new ReentrantLock();
        badLock.lock();
            for (int i = 0; i < 100000; i ++) {
                counter++;
            }
        badLock.unlock();
    }
}
