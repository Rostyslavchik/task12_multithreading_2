package com.rostyslavprotsiv.model.action;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class LockActionTest {

    public int testWithGlobalLock() {
        LockAction action = new LockAction();
        ExecutorService service = Executors.newFixedThreadPool(3);
        service.execute(action::incrC1);
        service.execute(action::incrC2);
        service.execute(action::incrC3);
        service.shutdown();
        try {
            service.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return action.getCounter();
    }

    public int testWithoutGlobalLock() {
        LockAction action = new LockAction();
        ExecutorService service = Executors.newFixedThreadPool(3);
        service.execute(action::incrC4);
        service.execute(action::incrC5);
        service.execute(action::incrC6);
        service.shutdown();
        try {
            service.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return action.getCounter();
    }
}
