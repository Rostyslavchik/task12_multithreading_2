package com.rostyslavprotsiv.model.entity.lock;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;

public class MyReadWriteLock implements ReadWriteLock {
    private SpinLock lock;
    private Lock writerLock;
    private Lock readerLock;
    private boolean writelockIsOn = false;
    private int numOfReadLocks = 0;
    private Thread currentWriterThread;
    private List<Thread> currentReaderThreads;

    public MyReadWriteLock() {
        writerLock = new WriteLock();
        readerLock = new ReadLock();
        lock = new SpinLock();
    }

    @Override
    public Lock readLock() {
        return readerLock;
    }

    @Override
    public Lock writeLock() {
        return writerLock;
    }

    private class WriteLock implements Lock {

        @Override
        public void lock() {
//            if (Thread.currentThread().equals(currentWriterThread)) {
//                return;
//            } I don't have much time to implement thread reentrance
            lock.lock();
            writelockIsOn = true;
//            currentWriterThread = Thread.currentThread();
        }

        @Override
        public void lockInterruptibly() throws InterruptedException {

        }

        @Override
        public boolean tryLock() {
            return false;
        }

        @Override
        public boolean tryLock(long l, TimeUnit timeUnit) throws InterruptedException {
            return false;
        }

        @Override
        public void unlock() {
            lock.unlock();
            writelockIsOn = false;
//            currentWriterThread = null;
        }

        @Override
        public Condition newCondition() {
            return null;
        }
    }

    private class ReadLock implements Lock {

        @Override
        public void lock() {
            if (lock.locked.get() && !writelockIsOn) {
                numOfReadLocks++;
//                currentReaderThreads.add(Thread.currentThread());
                return;
            }
            lock.lock();
            numOfReadLocks++;
//            currentReaderThreads.add(Thread.currentThread());
        }

        @Override
        public void lockInterruptibly() throws InterruptedException {

        }

        @Override
        public boolean tryLock() {
            return false;
        }

        @Override
        public boolean tryLock(long l, TimeUnit timeUnit) throws InterruptedException {
            return false;
        }

        @Override
        public void unlock() {
            if (numOfReadLocks > 1) {
                numOfReadLocks--;
//                currentReaderThreads.remove(Thread.currentThread());
                return;
            }
            lock.unlock();
            numOfReadLocks--;
//            currentReaderThreads.remove(Thread.currentThread());
        }

        @Override
        public Condition newCondition() {
            return null;
        }
    }

    private class SpinLock {
        private AtomicBoolean locked = new AtomicBoolean(false);

        void lock() {
            while (!locked.compareAndSet(false, true)) {}
        }

        void unlock() {
            locked.set(false);
        }
    }
}
