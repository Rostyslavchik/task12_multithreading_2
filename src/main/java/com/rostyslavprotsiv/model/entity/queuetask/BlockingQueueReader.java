package com.rostyslavprotsiv.model.entity.queuetask;

import java.sql.Time;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class BlockingQueueReader implements Runnable {
    private BlockingQueue<Character> blockingQueue;

    public BlockingQueueReader(BlockingQueue<Character> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        try {
            System.out.println(Thread.currentThread().getName() + " is reading");
            while (true) {
                TimeUnit.SECONDS.sleep(2);
                System.out.println(blockingQueue.take());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

//        StringBuilder result = new StringBuilder();
//        Character c;
//        try {
//            System.out.println(Thread.currentThread().getName() + " is reading");
//            while ((c = blockingQueue.poll(3, TimeUnit.SECONDS)) != null) {
//                result.append(c);
//            }
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        System.out.println(result);
    }
}
