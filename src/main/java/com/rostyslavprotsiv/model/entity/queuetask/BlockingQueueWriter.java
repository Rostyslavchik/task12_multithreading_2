package com.rostyslavprotsiv.model.entity.queuetask;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class BlockingQueueWriter implements Runnable {
    private BlockingQueue<Character> blockingQueue;
    private String message;

    public BlockingQueueWriter(BlockingQueue<Character> blockingQueue
            , String message) {
        this.blockingQueue = blockingQueue;
        this.message = message;
    }

    @Override
    public void run() {
            System.out.println(Thread.currentThread().getName() + " is writing");
            message.chars().forEach(i -> {
                try {
//                    TimeUnit.SECONDS.sleep(1);
                    blockingQueue.put((char) i);
                    System.out.println(Thread.currentThread().getName()
                            + " put" + (char) i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
    }
}
