package com.rostyslavprotsiv.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Menu {
    private static final Logger LOGGER = LogManager.getLogger(Menu.class);

    public void welcome() {
        LOGGER.info("Hi!! Welcome to my program!!");
    }

    public void outTestWithGlobalLock(int counter) {
        LOGGER.info("Out testing result with Lock in s class: " + counter);
    }

    public void outTestWithoutGlobalLock(int counter) {
        LOGGER.info("Out testing result with Lock in a method: " + counter);
    }

    public void outBlockingQueueCommunicationResult() {
        LOGGER.info("Out BlockingQueue-communication: ");
    }
}
